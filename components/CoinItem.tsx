import React, { useState } from "react";
import styles from "../styles/CoinItem.module.scss";

type CoinItemProps = {
  title: string;
  description: string;
  rate: string;
  rateFloat: number;
};

export const CoinItem: React.FC<CoinItemProps> = ({
  title,
  description,
  rate,
  rateFloat,
}) => {
  const [showInteger, setShowInteger] = useState(true);
  const voc = new Map();
  voc.set("EUR", 8364), voc.set("GBP", 163), voc.set("USD", 36);

  const changeRate = () => {
    setShowInteger(!showInteger);
  };

  return (
    <div className={styles.coinWrapper}>
      <div className={styles.title}>
        <div className={styles.title__text}>
          {title},{String.fromCharCode(voc.get(title))}
        </div>
        <div className={styles.title__description}>{description}</div>
      </div>
      <div onClick={changeRate} className={styles.rate}>
        {showInteger ? rate : rateFloat}
      </div>
    </div>
  );
};
