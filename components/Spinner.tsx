import React from "react";
import { ClipLoader } from "react-spinners";
import styles from "../styles/Spinner.module.scss";
export const Spinner = () => {
  return (
    <div className={styles.spinner__wrapper}>
      <ClipLoader />
    </div>
  );
};
