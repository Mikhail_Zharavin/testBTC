import type { NextPage } from "next";
import Head from "next/head";
import { CoinItem } from "../components/CoinItem";
import { useGetCoinsQuery } from "../store/coins/coins.api";
import { CurrencyDescription } from "../store/coins/coins.type";
import styles from "../styles/index.module.scss";
import Error from "../pages/Error";
import { Spinner } from "../components/Spinner";

const Home: NextPage = () => {
  const { data, isError, isLoading } = useGetCoinsQuery(undefined, {
    pollingInterval: 30000,
  });
  const abc = Object.entries(data?.bpi ? data?.bpi : {});
  let newArray: CurrencyDescription[] = [];
  abc.forEach(([key, value]) => {
    newArray.push(value);
  });

  if (isError) {
    return <Error />;
  }

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <div className={styles.index__wrapper}>
      <Head>
        <title>Главная Страница </title>
      </Head>
      <div className={styles.index__description}>
        <h1>{data?.chartName}</h1>
        <p>{data?.time.updated}</p>
      </div>
      <div className={styles.coin__items}>
        {newArray.map((element) => (
          <CoinItem
            key={element.code}
            title={element.code}
            description={element.description}
            rate={element.rate}
            rateFloat={element.rate_float}
          />
        ))}
      </div>
    </div>
  );
};

export default Home;
