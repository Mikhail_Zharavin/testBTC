import React from "react";
import styles from "../styles/Error.module.scss";
import { NextPage, NextPageContext } from "next";

const Error = () => {
  return (
    <div className={styles.error__wrapper}>
      <h1>Sorry, an error occurred, please try again.</h1>
    </div>
  );
};

export default Error;
