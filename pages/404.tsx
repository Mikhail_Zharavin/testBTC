import Image from "next/image";
import React from "react";
import img404 from "../images/Error.svg";
import styles from "../styles/404.module.scss";

export default function Custom404() {
  return (
    <div className={styles.custom404__wrapper}>
      <Image src={img404} alt="errorImage" />
      <h1>404 - Page Not Found</h1>
    </div>
  );
}
