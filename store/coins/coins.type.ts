interface Time {
  updated: string;
  updatedISO: string;
  updateduk: string;
}

export interface CurrencyDescription {
  code: string;
  symbol: string;
  rate: string;
  description: string;
  rate_float: number;
}

interface Coins {
  USD: CurrencyDescription;
  GBP: CurrencyDescription;
  EUR: CurrencyDescription;
}

export interface IData {
  time: Time;
  disclaimer: string;
  chartName: string;
  bpi: Coins;
}
