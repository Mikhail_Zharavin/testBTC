import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IData } from "./coins.type";

export const coinsApi = createApi({
  reducerPath: "api/coinsApi",
  tagTypes: ["Coins"],
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.coindesk.com/" }),
  endpoints: (build) => ({
    getCoins: build.query<IData, void>({
      query: () => "v1/bpi/currentprice.json",
      providesTags: () => ["Coins"],
    }),
  }),
});

export const { useGetCoinsQuery } = coinsApi;
